# Docker 101 Workshop Linux Day 2019 HTL Villach

This is a Docker 101 workshop which will cover the most basic things.

## Part 1: Docker Basics

### Starting a container

~~~
docker run hello-world
~~~

### Starting a Nginx webserver

~~~
docker run nginx
~~~

### Use dynamic network port

~~~
docker run -P nginx
~~~

### Use a fixed network port

~~~
docker run -p 80:80 nginx
~~~

### Docker execute
~~~
docker run -ti alpine bash
~~~

Run in background.
~~~
docker run --name linuxday -d nginx
~~~

Connect to running container.
~~~
docker ps
docker exec -ti linuxday bash 
~~~

### Containers are immutable
~~~
docker run --name linuxday -d nginx
docker exec -ti linuxday bash
touch test
ls --color
exit
docker stop linuxday
docker start linuxday
docker exec -ti linuxday bash
ls --color
exit
docker rm -f linuxday
docker run --name linuxday -d nginx
docker exec -ti linuxday bash
ls --color
exit
~~~

## Part 2: Docker images

For the next steps we need the repository.

~~~
git clone https://gitlab.com/n0r1sk/linuxday/docker-workshop.git
~~~

### Demo with dive
Demo with `dive` tool

### Create an image
Explain the Docker build command, and show demo.

~~~
cd docker-workshop
cd nginx-image
docker build . -t linuxday
docker run -P linuxday
~~~

### Inspect image
Inspect image
~~~
docker image inspect linxday
~~~

Inspect with `dive`
~~~
docker run --rm -it \
    -v /var/run/docker.sock:/var/run/docker.sock \
    wagoodman/dive:latest linuxday
~~~

### Delete image
~~~
docker image rm linuxday
~~~

## Part 3: Docker volumes
~~~
docker run -v /root/docker-workshop/nginx-image/content:/usr/share/nginx/html -P -d nginx
~~~

## Part 4: docker compose / docker swarm
~~~
cd wordpress-stack
docker-compose up
~~~

## Part 5: Advanced - Docker Swarm
Delete instance, recreate 3 managers 2 workers
~~~
docker swarm init
~~~

Clone GitLab repository

~~~
docker node ls
cd docker-workshop
cd wordpress-stack
docker stack deploy -c docker-compose.yml wordpress
docker service ls
docker stack ps wordpress
~~~

## Part 6: Ask me anything
